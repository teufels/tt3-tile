<?php
return [
    'ctrl' => [
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'editlock' => 'editlock',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'translationSource' => 'l10n_source',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'title' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element',
        'label' => 'element_backendtitle',
        'iconfile' => 'EXT:tt3_tile/Resources/Public/Icons/Element.svg',
        'hideTable' => true,
        'type' => 'record_type',
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'palettes' => [
        'language' => [
            'showitem' => '
                        sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l18n_parent
                    ',
        ],
        'hidden' => [
            'showitem' => '
                        hidden;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:field.default.hidden
                    ',
        ],
        'access' => [
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access',
            'showitem' => '
                        starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
                        endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel,
                        --linebreak--,
                        fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:fe_group_formlabel,
                        --linebreak--,editlock
                    ',
        ],
    ],
    'columns' => [
        'record_type' => [
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tt_content.record_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tt_content.record_type.I.0',
                        'value' => '0',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tt_content.record_type.I.1',
                        'value' => 'linked-element',
                    ],
                ],
            ],
        ],
        'editlock' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:editlock',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
            ],
        ],
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => ['type' => 'language'],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        'label' => '',
                        'value' => 0,
                    ],
                ],
                'default' => 0,
                'foreign_table' => 'tx_tt3tile_element',
                'foreign_table_where' => 'AND tx_tt3tile_element.pid=###CURRENT_PID### AND tx_tt3tile_element.sys_language_uid IN (-1, 0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        'label' => '',
                        'invertStateDisplay' => true,
                    ],
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'datetime',
                'default' => 0,
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => 2145913200,
                ],
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
        ],
        'fe_group' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.fe_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'size' => 5,
                'maxitems' => 20,
                'items' => [
                    [
                        'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hide_at_login',
                        'value' => -1,
                    ],
                    [
                        'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.any_login',
                        'value' => -2,
                    ],
                    [
                        'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.usergroups',
                        'value' => '--div--',
                    ],
                ],
                'exclusiveKeys' => '-1,-2',
                'foreign_table' => 'fe_groups',
            ],
        ],
        'parentid' => [
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        'label' => '',
                        'value' => 0,
                    ],
                ],
                'default' => 0,
                'foreign_table' => 'tt_content',
                'foreign_table_where' => 'AND tt_content.pid=###CURRENT_PID### AND tt_content.sys_language_uid IN (-1, ###REC_FIELD_sys_language_uid###)',
            ],
        ],
        'parenttable' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'sorting' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'element_backendtitle' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => '0',
                ],
                'type' => 'input',
            ],
            'description' => 'Backendtitle of the Element',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_backendtitle',
            'order' => 1,
        ],
        'element_bgcolor' => [
            'config' => [
                'items' => [
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.0',
                        'value' => 'transparent',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.1',
                        'value' => 'primary',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.2',
                        'value' => 'secondary',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.3',
                        'value' => 'tertiary',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.4',
                        'value' => 'quaternity',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.5',
                        'value' => 'white',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.6',
                        'value' => 'light',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.7',
                        'value' => 'dark',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor.I.8',
                        'value' => 'danger',
                    ],
                ],
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgcolor',
            'order' => 9,
        ],
        'element_bgimage' => [
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'sys_file_reference',
                'foreign_field' => 'uid_foreign',
                'foreign_sortby' => 'sorting_foreign',
                'foreign_table_field' => 'tablenames',
                'foreign_match_fields' => [
                    'fieldname' => 'element_bgimage',
                ],
                'foreign_label' => 'uid_local',
                'foreign_selector' => 'uid_local',
                'overrideChildTca' => [
                    'columns' => [
                        'uid_local' => [
                            'config' => [
                                'appearance' => [
                                    'elementBrowserType' => 'file',
                                    'elementBrowserAllowed' => 'gif,jpg,jpeg,png,svg',
                                ],
                            ],
                        ],
                    ],
                    'types' => [
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                    ],
                ],
                'filter' => [
                    [
                        'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                        'parameters' => [
                            'allowedFileExtensions' => 'gif,jpg,jpeg,png,svg',
                        ],
                    ],
                ],
                'appearance' => [
                    'useSortable' => false,
                    'enabledControls' => [
                        'info' => true,
                        'new' => false,
                        'dragdrop' => true,
                        'sort' => false,
                        'hide' => true,
                        'delete' => true,
                    ],
                ],
                'maxitems' => '1',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgimage',
            'order' => 7,
        ],
        'element_bgimageposition' => [
            'config' => [
                'default' => 'center center',
                'eval' => 'trim,lower',
                'type' => 'input',
            ],
            'description' => 'CSS background-position Property
e.g.: "center center", "x% y%", "xpos ypos"',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_bgimageposition',
            'order' => 8,
        ],
        'element_class' => [
            'config' => [
                'eval' => 'trim,lower',
                'type' => 'input',
            ],
            'description' => 'Use for example bootstrap .order- classes for controlling the visual order of the element. 
(e.g.: order-1 order-lg-2) or use bootstrap display classes to show/hide element (e.g.: d-none d-lg-block)',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_class',
            'order' => 11,
        ],
        'element_icon' => [
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'sys_file_reference',
                'foreign_field' => 'uid_foreign',
                'foreign_sortby' => 'sorting_foreign',
                'foreign_table_field' => 'tablenames',
                'foreign_match_fields' => [
                    'fieldname' => 'element_icon',
                ],
                'foreign_label' => 'uid_local',
                'foreign_selector' => 'uid_local',
                'overrideChildTca' => [
                    'columns' => [
                        'uid_local' => [
                            'config' => [
                                'appearance' => [
                                    'elementBrowserType' => 'file',
                                    'elementBrowserAllowed' => 'gif,jpg,jpeg,png,svg',
                                ],
                            ],
                        ],
                    ],
                    'types' => [
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                    ],
                ],
                'filter' => [
                    [
                        'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                        'parameters' => [
                            'allowedFileExtensions' => 'gif,jpg,jpeg,png,svg',
                        ],
                    ],
                ],
                'appearance' => [
                    'useSortable' => false,
                    'enabledControls' => [
                        'info' => true,
                        'new' => false,
                        'dragdrop' => true,
                        'sort' => false,
                        'hide' => true,
                        'delete' => true,
                    ],
                ],
                'maxitems' => '1',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_icon',
            'order' => 6,
        ],
        'element_titletype' => [
            'config' => [
                'items' => [
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.default',
                        'value' => '',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.I.0',
                        'value' => 'h1',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.I.1',
                        'value' => 'h2',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.I.2',
                        'value' => 'h3',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.I.3',
                        'value' => 'h4',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.I.4',
                        'value' => 'h5',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.I.5',
                        'value' => 'h6',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.I.6',
                        'value' => 'strong',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype.I.7',
                        'value' => 'label',
                    ],
                ],
                'maxitems' => '1',
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titletype',
            'order' => 3,
        ],
        'element_titleposition' => [
            'config' => [
                'items' => [
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titleposition.default',
                        'value' => '',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titleposition.I.0',
                        'value' => 'text-left text-start',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titleposition.I.1',
                        'value' => 'text-center',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titleposition.I.2',
                        'value' => 'text-right text-end',
                    ],
                ],
                'maxitems' => '1',
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_titleposition',
            'order' => 3,
        ],
        'element_link' => [
            'config' => [
                'type' => 'link',
                'appearance' => ['browserTitle' => 'Link'],
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_link',
            'order' => 5,
        ],
        'element_size' => [
            'config' => [
                'items' => [
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_size.I.0',
                        'value' => 'col-12',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_size.I.1',
                        'value' => 'col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_size.I.2',
                        'value' => 'col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_size.I.3',
                        'value' => 'col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4',
                    ],
                    [
                        'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_size.I.4',
                        'value' => 'col-12 col-sm-12 col-md-12 col-lg-6 col-xl-3',
                    ],
                ],
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_size',
            'order' => 10,
        ],
        'element_text' => [
            'config' => [
                'enableRichtext' => '1',
                'softref' => 'rtehtmlarea_images,typolink_tag,images,email[subst],url',
                'type' => 'text',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_text',
            'order' => 4,
        ],
        'element_title' => [
            'config' => [
                'placeholder' => 'Title',
                'type' => 'input',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tx_tt3tile_element.element_title',
            'order' => 2,
        ],
        't3_origuid' => [
            'config' => [
                'type' => 'passthrough',
                'default' => 0,
            ],
        ],
        'l10n_source' => [
            'config' => [
                'type' => 'passthrough',
                'default' => 0,
            ],
        ],
    ],
    'types' => [
        1 => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,record_type,element_backendtitle,element_title,element_titletype,element_titleposition,element_text,element_link,element_icon,element_bgimage,element_bgimageposition,element_bgcolor,element_size,element_class,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;;access',
        ],
        'linked-element' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,record_type,element_backendtitle,element_link,element_title,element_titletype,element_titleposition,element_text,element_icon,element_bgimage,element_bgimageposition,element_bgcolor,element_size,element_class,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;;access',
            'columnsOverrides' => [
                'element_link' => [
                    'config' => [
                        'eval' => 'required',
                    ],
                ],
            ],
        ],
    ],
];