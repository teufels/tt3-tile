<?php
defined('TYPO3') or defined('TYPO3') or die();

$extensionKey = 'tt3_tile';
$extensionTitle = '[teufels] Tile';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', $extensionTitle);