<?php
defined('TYPO3') or defined('TYPO3') or die();

call_user_func(function () {

    //Adding Custom CType Item Group
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItemGroup(
        'tt_content',
        'CType',
        'teufels',
        'teufels',
        'after:special'
    );

    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['tt3tile_tt3_tile'] = 'tt3tile_plugin_icon';
    $tempColumns = [
        'backendtitle' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
                'type' => 'input',
            ],
            'description' => 'Backend Title (not visible in frontend)',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tt_content.backendtitle',
        ],
        'tx_tt3tile_element' => [
            'config' => [
                'appearance' => [
                    'enabledControls' => [
                        'dragdrop' => '1',
                    ],
                    'levelLinksPosition' => 'both',
                    'useSortable' => '1',
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'foreign_field' => 'parentid',
                'foreign_sortby' => 'sorting',
                'foreign_table' => 'tx_tt3tile_element',
                'foreign_table_field' => 'parenttable',
                'type' => 'inline',
                'minitems' => 1,
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3tile_element',
        ],
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);

    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        // Label
        'LLL:EXT:tt3_tile/Resources/Private/Language/locallang_db.xlf:tt_content.CType.tt3tile_tt3_tile',
        // Value written to the database
        'tt3tile_tt3_tile',
        // Icon
        'tt3tile_plugin_icon',
        // The group ID, if not given, falls back to "none" or the last used --div-- in the item array
        'teufels'
    ];
    $tempTypes = [
        'tt3tile_tt3_tile' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,backendtitle,tx_tt3tile_element,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames, --palette--;LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.palette.backgroundimage4ce;backgroundimage4ce,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
        ],
    ];
    $GLOBALS['TCA']['tt_content']['types'] += $tempTypes;

    /**
     * Preview Renderer (needed for TYPO3 v12 backwards compatibility)
     */
    $GLOBALS['TCA']['tt_content']['types']['tt3tile_tt3_tile']['previewRenderer'] = \Teufels\Tt3Tile\Preview\PreviewRenderer::class;
});

