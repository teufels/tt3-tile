CREATE TABLE tt_content (
    backendtitle varchar(255) DEFAULT '' NOT NULL,
    tx_tt3tile_element int(11) unsigned DEFAULT '0' NOT NULL
);
CREATE TABLE tx_tt3tile_element (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    element_backendtitle tinytext,
    element_bgcolor tinytext,
    element_bgimage int(11) unsigned DEFAULT '0' NOT NULL,
    element_bgimageposition tinytext,
    element_class tinytext,
    element_icon int(11) unsigned DEFAULT '0' NOT NULL,
    element_titletype tinytext,
    element_titleposition tinytext,
    element_link tinytext,
    element_size tinytext,
    element_text mediumtext,
    element_title tinytext,
    record_type varchar(100) NOT NULL DEFAULT '0',
    KEY language (l10n_parent,sys_language_uid)
);
