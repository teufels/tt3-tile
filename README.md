[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--tile-orange.svg)](https://bitbucket.org/teufels/tt3-tile/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__tile-red.svg)](https://bitbucket.org/teufels/tt3-tile/src/main/)
![version](https://img.shields.io/badge/version-1.0.*-yellow.svg?style=flat-square)

[ ṯeufels ] Tile
==========
Tile Content-Element which can contain Image, BG-Color, Title, Text, Link

#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/tt3-tile`

***

### Requirements
- `none`

***

### How to use
- Install with composer
- Import Static Template (before sitepackage)
- make own Layout by override Template in sitepackage

***

### Update & Migration from hive_tile
1. in composer.json replace `beewilly/hive_tile` with `"teufels/tt3-tile":"^1.0"`
2. Composer update
3. Include TypoScript set `[teufels] Tile`
4. Analyze Database Structure -> Add tables & fields (do not remove old hive_tile yet)
5. Perform Upgrade Wizards `[teufels] Tile`
6. Analyze Database Structure -> Remove tables & unused fields (remove old hive_tile now)
7. class & id changed -> adjust styling in sitepackage (e.g. tx_hive_tile => tx_tt3_tile)
8. check & adjust be user group access rights

***

### Changelog
#### 1.1.x
- 1.1.1 custom preview renderer only used for TYPO3 v12, TYPO3 v13 uses default renderer & add Previews for TYPO3 v13 and TYPO3 v12 support
- 1.1.0 add support for TYPO3 v13
#### 1.0.x
- 1.0.3 fix renderContentElementPreviewFromFluidTemplate changed parameters
- 1.0.2 change c-<id> to c<id> & improve BE Preview
- 1.0.1 changed deprecated allowTableOnStandardPages() to ignorePageTypeRestriction (https://docs.typo3.org/m/typo3/reference-tca/12.4/en-us/Ctrl/Properties/Security.html#ctrl-security-ignorepagetyperestriction)
- 1.0.0 intial from [hive_tile](https://bitbucket.org/teufels/hive_tile/src/)