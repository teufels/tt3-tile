<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_tile" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Tile\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3tileDataUpdater')]
class DataUpdater implements UpgradeWizardInterface
{

    public function getTitle(): string
    {
        return '[teufels] Tile: Migrate data';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all data from tx_hivetile_tileelement to the new tx_tt3tile_element';
        if($this->checkMigrationTableExist()) { $description .= ': ' . count($this->getMigrationRecords()); }
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class,
        ];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return $this->checkMigrationTableExist();
    }

    public function performMigration(): bool
    {
        //data
        $sql = "INSERT INTO tx_tt3tile_element(uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,fe_group,editlock,sys_language_uid,l10n_parent,l10n_source,t3_origuid,parentid,parenttable,sorting,element_backendtitle,element_bgcolor,element_bgimage,element_bgimageposition,element_class,element_icon,element_titletype,element_titleposition,element_link,element_size,element_text,element_title,record_type) 
        SELECT uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,fe_group,editlock,sys_language_uid,l10n_parent,l10n_source,t3_origuid,parentid,parenttable,sorting,tx_hivetile_elementbackendtitle,tx_hivetile_elementbgcolor,tx_hivetile_elementbgimage,tx_hivetile_elementbgimageposition,tx_hivetile_elementclass,tx_hivetile_elementicon,tx_hivetile_elementitletype,tx_hivetile_elementitleposition,tx_hivetile_elementlink,tx_hivetile_elementsize,tx_hivetile_elementtext,tx_hivetile_elementtitle,record_type
        FROM tx_hivetile_tileelement WHERE deleted = 0";

        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_tt3tile_element');
        /** @var DriverStatement $statement */
        $statement = $connection->prepare($sql);
        $statement->execute();

        //sys_file_reference
        $old = 'tx_hivetile_element';
        $new = 'element_';

        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('sys_file_reference');

        $records = $queryBuilder
            ->select('uid', 'fieldname')
            ->from('sys_file_reference')
            ->where(
                $queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_hivetile_tileelement'))
            )
            ->executeQuery()
            ->fetchAllAssociative();

        foreach ($records as $record) {
            $newFieldname = str_replace($old, $new, $record['fieldname']);

            $queryBuilder
                ->update('sys_file_reference')
                ->set('tablenames', 'tx_tt3tile_element')
                ->set('fieldname', $newFieldname)
                ->where(
                    $queryBuilder->expr()->in(
                        'uid',
                        $queryBuilder->createNamedParameter($record['uid'], Connection::PARAM_INT)
                    )
                )
                ->executeStatement();
        }

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_hivetile_tileelement');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid')
            ->from('tx_hivetile_tileelement')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function checkMigrationTableExist(): bool {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_hivetile_tileelement')
            ->getSchemaManager()
            ->tablesExist(['tx_hivetile_tileelement']);
    }

}
