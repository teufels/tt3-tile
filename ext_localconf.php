<?php
defined('TYPO3') or defined('TYPO3') or die();

call_user_func(function () {

    /**
     * Register icons
     */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'tt3tile_plugin_icon',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        [
            'source' => 'EXT:tt3_tile/Resources/Public/Icons/Plugin.svg',
        ]
    );

});

